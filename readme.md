# 创建分支
```
git branch 分支名
git checkout -b 分支名   //创建本地分支并切换
```
# 查看分支
```
git branch -a  //查看所有分支
git branch  //查看当前分支
```
# 切换分支
```
git checkout 分支名
```
# 推送到远程分支
```
git push origin 远程分支名
```
# 合并本地分支
```
git checkout master
git merge 分支名
```
